import argparse
import fileinput
import glob
import json
import os
import re

from wordcloud import WordCloud

CODECLOUD_TEMP_TXT = "codecloud-temp.txt"


def parse_args():
    parser = argparse.ArgumentParser(description='Create a word cloud from source code.')
    parser.add_argument('--ruleset', default='cpp')
    parser.add_argument('--directories', nargs='+')
    return parser.parse_args()


def load_config(ruleset):
    with open('rulesets/{}.json'.format(ruleset)) as f:
        return json.load(f)


def apply_replacements(line, replacements):
    replaced = line
    for replacement in replacements:
        replaced = re.sub(replacement["from"], replacement["to"], replaced)
    return replaced.lower()


def find_all_filenames(directories, file_types):
    filenames = []
    for directory in directories:
        for file_type in file_types:
            filenames.extend(glob.glob('{}/**/*{}'.format(directory, file_type), recursive=True))
    return filenames


def process_and_write(filenames, replacements):
    with open(CODECLOUD_TEMP_TXT, "w") as merged_filename:
        for line in fileinput.input(filenames):
            merged_filename.write(apply_replacements(line, replacements))


def create_wordcloud(stopwords):
    wc = WordCloud(collocations=False, width=1000, height=500, stopwords=stopwords)

    with open(CODECLOUD_TEMP_TXT) as f:
        wc.generate(f.read())

    image = wc.to_image()
    image.show()


def remove_temporaries():
    os.remove(CODECLOUD_TEMP_TXT)


if __name__ == '__main__':
    args = parse_args()
    config = load_config(args.ruleset)

    all_filenames = find_all_filenames(args.directories, config["file_types"])
    process_and_write(all_filenames, config["replacements"])

    create_wordcloud(config["remove_words"])

    remove_temporaries()
